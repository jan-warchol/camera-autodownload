UWAGA!  Nie należy edytować tych plików windowsowym notatnikiem, ponieważ
nie ma on dobrego wsparcia dla użytego kodowania.  Należy użyć inteligentnego
programu jak np. http://notepad-plus-plus.org/


Co to jest
----------

Program do automatycznego pobierania nagrań z kamer przemysłowych.



Założenia
---------

- każda kamera udostępnia foldery z nagraniami po HTTP
- pliki się nie zmieniają, są tylko dodawane nowe



Przygotowanie środowiska
------------------------

Ponieważ program jest zrealizowany jako skrypt bashowy (powłoki Linuxowej),
żeby używać go pod Windowsem należy zainstalować środowisko linuxowe Cygwin
(https://cygwin.com/install.html) z następującymi pakietami:
- wget

Oprócz tego do pobrania i aktualizowania samego programu jest potrzebny Git
(http://www.git-scm.com/download/win).



Instalacja
----------

Program można pobrać z repozytorium gitowego znajdującego się pod adresem
https://bitbucket.org/janek-warchol/camera-autodownload

Aby kopiowanie nagrań następowało automatycznie, należy utworzyć zadanie
w windowsowym harmonogramie zadań.  Można w tym celu skorzystać z pliku
`przykładowe-zadanie.xml` i zaimportować go do harmonogramu.

To przykładowe zadanie zakłada, że
- Cygwin jest zainstalowany w `C:\cygwin`
- ścieżka do programu (w notacji unixowej, zaczynając od litery napędu) to
  f/kamera-autodownload/skrypt-pobierający/download-recordings.sh
- program ma być uruchamiany o każdej pełnej godzinie



Konfiguracja
------------

Katalog, do którego są kopiowane nagrania, oraz adresy IP z których są one
pobierane, można ustawić edytując sam skrypt.

Należy do tego użyć porządnego edytora tekstu, NIE WINDOWSOWEGO NOTATNIKA!



Aktualizowanie
--------------

1. Otworzyć katalog z programem w eksploratorze plików
2. Uruchomić program "Git bash" korzystając z menu kontekstowego
3. Wpisać polecenie "git pull" (bez cudzysłowu)
4. Jeśli pojawi się informacja o konfliktach, skontaktować się z administratorem

