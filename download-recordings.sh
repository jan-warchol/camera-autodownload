﻿#!/bin/bash

set -o errexit
set -o nounset

# configuration
OUT_DIR=/cygdrive/f/kamera-autodownload/nagrania
LOG_DIR=/cygdrive/f/kamera-autodownload/logi

IP_ADDRESSES=(
    "192.168.1.80"
    "192.168.1.81"
    "192.168.1.82"
    "192.168.1.83"
    "192.168.1.84"
    "192.168.1.85"
    "192.168.1.86"
    "192.168.1.87"
)

# setup
mkdir -p $OUT_DIR
mkdir -p $LOG_DIR

# main logic
download_from() {
    IP_ADDRESS=$1
    LOG_PATH=$LOG_DIR/wget-$IP_ADDRESS-$(date +"%Y-%m-%d_%H-%M").log

    # force wget to download updated indices despite --continue flag
    echo "Removing outdated indices:" > $LOG_PATH
    find $OUT_DIR -name index.html -delete -print >> $LOG_PATH
    echo "" >> $LOG_PATH

    echo "Downloading recursively from $IP_ADDRESS..." >> $LOG_PATH
    wget \
    --execute robots=off \
    --user=admin \
    --password=123456 \
    --no-clobber \
    --recursive \
    --no-parent \
    --no-verbose \
    --level=10 \
    --directory-prefix=$OUT_DIR \
    --cut-dirs=1 \
    http://$IP_ADDRESS/sd/ \
    &>> $LOG_PATH
}

for ip in ${IP_ADDRESSES[*]}; do
    download_from $ip
done
